package com.example.mercadolibreprueba.presenters;

import android.util.Log;

import com.example.mercadolibreprueba.R;
import com.example.mercadolibreprueba.models.SearchResponse.SearchResultProduct;
import com.example.mercadolibreprueba.models.SearchResponse.SearchResponse;
import com.example.mercadolibreprueba.network.ApiClient;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchPresenter {

    private final String TAG = "searchPresenter";
    private final int MIN_INPUT_LENGTH = 3;

    private Listener listener;


    public SearchPresenter(Listener listener) {
        this.listener = listener;
    }

    public void search(String inputText) {
        if (inputText.length() < MIN_INPUT_LENGTH) return;

        listener.showLoading();
        ApiClient.getApiClient().search(inputText).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                listener.hideLoading();
                if (!response.isSuccessful() || response.body().getResults().isEmpty()) {
                    listener.onSomethingWentWrong(R.string.not_found);
                    return;
                }
                listener.onSearchResult(response.body().getResults());
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                listener.onSomethingWentWrong(R.string.internet_missing);
                Log.e(TAG, t.toString());
            }
        });
    }

    public void productSelected(SearchResultProduct searchResultProduct) {
        listener.onInfoProduct(searchResultProduct);
    }

    public interface Listener {
        void onSomethingWentWrong(int messageId);
        void onSearchResult(List<SearchResultProduct> searchResultProductList);
        void onInfoProduct(SearchResultProduct searchResultProduct);
        void showLoading();
        void hideLoading();
    }

}
