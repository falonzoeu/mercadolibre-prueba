package com.example.mercadolibreprueba.presenters;

import com.example.mercadolibreprueba.models.SearchResponse.SearchResultProduct;

public class ProductPresenter {

    private Listener listener;
    private SearchResultProduct selectedSearchResultProduct;

    public ProductPresenter(Listener listener) {
        this.listener = listener;
    }

    public void setSearchResultProduct(SearchResultProduct searchResultProduct) {
        this.selectedSearchResultProduct = searchResultProduct;
    }

    public void goToMeliPressed() {
        listener.onGoToMeli(selectedSearchResultProduct.getPermalink());
    }

    public interface Listener {
        void onGoToMeli(String productUrl);
    }

}
