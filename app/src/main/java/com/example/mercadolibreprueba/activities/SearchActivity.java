package com.example.mercadolibreprueba.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mercadolibreprueba.R;
import com.example.mercadolibreprueba.adapters.SearchResultAdapter;
import com.example.mercadolibreprueba.models.SearchResponse.SearchResultProduct;
import com.example.mercadolibreprueba.presenters.SearchPresenter;
import com.example.mercadolibreprueba.ui.SearchMeliToolbar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity implements
        SearchMeliToolbar.Listener,
        SearchPresenter.Listener,
        SearchResultAdapter.Listener {

    @BindView(R.id.activity_main_meliToolbar) SearchMeliToolbar searchMeliToolbar;
    @BindView(R.id.activity_main_recycler) RecyclerView recyclerView;
    @BindView(R.id.activity_main_not_found) TextView notFoundText;
    @BindView(R.id.activity_main_progressbar) ProgressBar progressBar;

    private SearchPresenter presenter;
    private SearchResultAdapter searchResultAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new SearchPresenter(this);
        searchMeliToolbar.setListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchResultAdapter = new SearchResultAdapter(this, getBaseContext());
        recyclerView.setAdapter(searchResultAdapter);
    }

    /**SearchMeliToolbar.Listener */
    @Override
    public void onTextChanged(String inputText) {
        presenter.search(inputText);
    }

    /**SearchResultAdapter.Listener*/
    @Override
    public void onSelectedProduct(SearchResultProduct searchResultProduct) {
        presenter.productSelected(searchResultProduct);
    }

    /**SearchPresenter.Listener */
    @Override
    public void onSomethingWentWrong(int messageId) {
        searchResultAdapter.clear();
        progressBar.setVisibility(View.GONE);
        notFoundText.setVisibility(View.VISIBLE);
        notFoundText.setText(getString(messageId));
    }

    @Override
    public void onSearchResult(List<SearchResultProduct> searchResultProductList) {
        if (notFoundText.getVisibility() != View.GONE)
            notFoundText.setVisibility(View.GONE);
        if (recyclerView.getVisibility() != View.VISIBLE)
            recyclerView.setVisibility(View.VISIBLE);

        searchResultAdapter.setSearchResultProductList(searchResultProductList);
    }

    @Override
    public void onInfoProduct(SearchResultProduct searchResultProduct) {
        ProductActivity.start(this, searchResultProduct);
        overridePendingTransition(R.anim.slide_in, R.anim.no_animation);
    }

    @Override
    public void showLoading() {
        runOnUiThread(() -> {
            recyclerView.setVisibility(View.GONE);
            notFoundText.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        });
    }

    @Override
    public void hideLoading() {
        runOnUiThread(() -> {
            progressBar.setVisibility(View.GONE);
        });
    }
}
