package com.example.mercadolibreprueba.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.mercadolibreprueba.R;
import com.example.mercadolibreprueba.models.SearchResponse.Address;
import com.example.mercadolibreprueba.models.SearchResponse.SearchResultProduct;
import com.example.mercadolibreprueba.presenters.ProductPresenter;
import com.example.mercadolibreprueba.ui.AdapterLinearLayout;
import com.example.mercadolibreprueba.ui.BackMeliToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductActivity extends AppCompatActivity implements
        ProductPresenter.Listener,
        BackMeliToolbar.Listener {

    private static final String PRODUCT_KEY = "product_key";

    public static void start(Context context, SearchResultProduct searchResultProduct) {
        Intent intent = new Intent(context, ProductActivity.class);
        intent.putExtra(PRODUCT_KEY, searchResultProduct);
        context.startActivity(intent);
    }

    @BindView(R.id.activity_product_toolbar) BackMeliToolbar toolbar;
    @BindView(R.id.activity_product_title) TextView title;
    @BindView(R.id.activity_product_imageview) ImageView imageView;
    @BindView(R.id.activity_product_price) TextView price;
    @BindView(R.id.activity_product_place) TextView place;
    @BindView(R.id.activity_product_caracteristics_list) AdapterLinearLayout adapterLinearLayout;

    private ProductPresenter productPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);

        toolbar.setListener(this);

        SearchResultProduct selectedSearchResultProduct = (SearchResultProduct) getIntent().getExtras().getSerializable(PRODUCT_KEY);
        loadUI(selectedSearchResultProduct);

        productPresenter = new ProductPresenter(this);
        productPresenter.setSearchResultProduct(selectedSearchResultProduct);
    }

    @OnClick(R.id.activity_product_open_meli)
    public void meliButtonPress(View view) {
        productPresenter.goToMeliPressed();
    }

    /**BackMeliToolbar.Listener*/
    @Override
    public void onBackButtonPress() {
        onBackPressed();
    }

    /**ProductPresenter.Listener*/
    @Override
    public void onGoToMeli(String productUrl) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(productUrl));
        startActivity(browserIntent);
    }

    /**Private methods*/
    private void loadUI(SearchResultProduct searchResultProduct) {
        Glide.with(this)
                .load(searchResultProduct.getThumbnail())
                .into(imageView);
        title.setText(searchResultProduct.getTitle());
        price.setText(String.format(getString(R.string.price), String.valueOf(searchResultProduct.getPrice())));
        Address address = searchResultProduct.getAddress();
        place.setText(String.format(getString(R.string.place1_place2), address.getStateName(), address.getCityName()));
        adapterLinearLayout.setAttributesList(searchResultProduct.getAttributes());
    }
}
