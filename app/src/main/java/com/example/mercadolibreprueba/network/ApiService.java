package com.example.mercadolibreprueba.network;

import com.example.mercadolibreprueba.models.SearchResponse.SearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("sites/MLA/search")
    Call<SearchResponse> search(@Query("q") String query);
}
