package com.example.mercadolibreprueba.network;

import com.example.mercadolibreprueba.BuildConfig;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static ApiService apiService;

    public static ApiService getApiClient() {
        if (apiService==null)
            apiService = buildApiClient();
        return apiService;
    }

    private static ApiService buildApiClient() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .baseUrl(BuildConfig.MELI_BASE_URL)
                .client(new OkHttpClient.Builder()
                        .addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                        .build()
                )
                .build()
                .create(ApiService.class);

    }
}
