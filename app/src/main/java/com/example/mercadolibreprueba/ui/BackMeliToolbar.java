package com.example.mercadolibreprueba.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.example.mercadolibreprueba.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class BackMeliToolbar extends FrameLayout {

    private Listener listener;

    public BackMeliToolbar(Context context) {
        this(context, null);
    }
    public BackMeliToolbar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public BackMeliToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.back_meli_toolbar, this);
        ButterKnife.bind(this);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @OnClick(R.id.meli_toolbar_back_button)
    public void onBackPress(View view) {
        listener.onBackButtonPress();
    }

    public interface Listener {
        void onBackButtonPress();
    }
}
