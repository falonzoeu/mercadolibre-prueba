package com.example.mercadolibreprueba.ui;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.example.mercadolibreprueba.R;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchMeliToolbar extends FrameLayout {

    @BindView(R.id.meli_toolbar_search) EditText searchText;

    private final int INPUT_DELAY = 600;

    private Listener listener;
    private Timer timer;

    public SearchMeliToolbar(Context context) {
        this(context, null);
    }
    public SearchMeliToolbar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public SearchMeliToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.search_meli_toolbar, this);
        ButterKnife.bind(this);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //nothing to do
            }

            //To not doing a search request for each input change. I am setting a delay to reduce the number of requests.
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (timer != null)
                    timer.cancel();
            }

            // reset the timer if the user is still typing
            @Override
            public void afterTextChanged(Editable editable) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        listener.onTextChanged(editable.toString());
                    }
                }, INPUT_DELAY);
            }
        });
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        void onTextChanged(String inputText);
    }
}
