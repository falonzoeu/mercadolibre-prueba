package com.example.mercadolibreprueba.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mercadolibreprueba.R;
import com.example.mercadolibreprueba.models.Attributes;

import java.util.List;

public class AdapterLinearLayout extends LinearLayout {

    public AdapterLinearLayout(Context context) {
        this(context, null);
    }
    public AdapterLinearLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public AdapterLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(VERTICAL);
    }

    public void setAttributesList(List<Attributes> attributesList) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        for (Attributes attribute : attributesList) {
            View view  = inflater.inflate(R.layout.attribute_item, this, false);

            TextView txtAttributeName = view.findViewById(R.id.attribute_item_name);
            txtAttributeName.setText(String.format(getContext().getString(R.string.attribute_name), attribute.getName()));

            TextView txtValue = view.findViewById(R.id.attribute_item_value);
            txtValue.setText(attribute.getValueName());

            addView(view);
        }
    }
}
