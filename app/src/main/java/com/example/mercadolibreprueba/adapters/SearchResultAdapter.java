package com.example.mercadolibreprueba.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mercadolibreprueba.R;
import com.example.mercadolibreprueba.models.SearchResponse.SearchResultProduct;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchResultAdapter extends RecyclerView.Adapter {

    private List<SearchResultProduct> searchResultProductList;
    private Listener listener;
    private Context context;

    public SearchResultAdapter(Listener listener, Context context) {
        this.listener = listener;
        this.context = context;
        searchResultProductList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_result_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder)holder).loadData(searchResultProductList.get(position));
    }

    @Override
    public int getItemCount() {
        return searchResultProductList.size();
    }

    public void setSearchResultProductList(List<SearchResultProduct> searchResultProductList) {
        this.searchResultProductList = searchResultProductList;
        notifyDataSetChanged();
    }

    public void clear() {
        setSearchResultProductList(new ArrayList<>());
    }

    public interface Listener {
        void onSelectedProduct(SearchResultProduct searchResultProduct);
    }

    /**ViewHolder*/
    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.result_item_image_view) ImageView image;
        @BindView(R.id.result_item_place) TextView placeText;
        @BindView(R.id.result_item_price) TextView priceText;
        @BindView(R.id.result_item_title) TextView titleText;

        private SearchResultProduct searchResultProduct;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener((View v) -> {
                listener.onSelectedProduct(searchResultProduct);
            });
        }

        void loadData(SearchResultProduct searchResultProduct) {
            this.searchResultProduct = searchResultProduct;
            Glide
                    .with(itemView)
                    .load(searchResultProduct.getThumbnail())
                    .centerCrop()
                    .into(image);
            titleText.setText(searchResultProduct.getTitle());
            priceText.setText(String.format(context.getString(R.string.price) ,String.valueOf(searchResultProduct.getPrice())));
            placeText.setText(searchResultProduct.getAddress().getCityName());
        }
    }

}
