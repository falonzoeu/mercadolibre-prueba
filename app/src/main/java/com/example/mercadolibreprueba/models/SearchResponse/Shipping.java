package com.example.mercadolibreprueba.models.SearchResponse;

import java.io.Serializable;

class Shipping implements Serializable {

    private String mode;

    private boolean
            free_shipping,
            store_pick_up;

    public String getMode() {
        return mode;
    }

    public boolean hasFreeShipping() {
        return free_shipping;
    }

    public boolean canPickUpInStore() {
        return store_pick_up;
    }
}
