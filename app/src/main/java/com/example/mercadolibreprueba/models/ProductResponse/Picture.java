package com.example.mercadolibreprueba.models.ProductResponse;

public class Picture {

    private String
            id,
            url,
            max_width,
            max_height;

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getMaxWidth() {
        return max_width;
    }

    public String getMaxHeight() {
        return max_height;
    }
}
