package com.example.mercadolibreprueba.models.ProductResponse;

public class ShortDescription {

    private String
            type,
            content;

    public String getType() {
        return type;
    }

    public String getContent() {
        return content;
    }
}
