package com.example.mercadolibreprueba.models.ProductResponse;

import com.example.mercadolibreprueba.models.BaseResponse;

import java.util.List;

public class Product extends BaseResponse {

    private String
            id,
            active,
            permalink,
            name,
            parent_id,
            status;

    private List<Picture> pictures;
    private ShortDescription short_description;

    public String getId() {
        return id;
    }

    public String getActive() {
        return active;
    }

    public String getPermalink() {
        return permalink;
    }

    public String getName() {
        return name;
    }

    public String getParentId() {
        return parent_id;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public ShortDescription getShortDescription() {
        return short_description;
    }

    public String getStatus() {
        return status;
    }
}