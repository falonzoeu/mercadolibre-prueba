package com.example.mercadolibreprueba.models;

public abstract class BaseResponse {

    private String
            message,
            error;

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }

}
