package com.example.mercadolibreprueba.models.SearchResponse;

import java.io.Serializable;

public class Position implements Serializable {

    private String
            id,
            name;

    public Position(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
