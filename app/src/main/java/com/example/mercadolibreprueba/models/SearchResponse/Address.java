package com.example.mercadolibreprueba.models.SearchResponse;

import java.io.Serializable;

public class Address implements Serializable {

    private String state_id;
    private String state_name;
    private String city_name;

    public Address(String state_id, String state_name, String city_name) {
        this.state_id = state_id;
        this.state_name = state_name;
        this.city_name = city_name;
    }

    public String getStateId() {
        return state_id;
    }

    public String getStateName() {
        return state_name;
    }

    public String getCityName() {
        return city_name;
    }
}
