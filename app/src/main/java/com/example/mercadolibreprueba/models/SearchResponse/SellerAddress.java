package com.example.mercadolibreprueba.models.SearchResponse;

import java.io.Serializable;

class SellerAddress implements Serializable {

    private String
            comment,
            address_line,
            zip_code,
            latitude,
            longitude;

    private Position
            country,
            city,
            state;

    public String getComment() {
        return comment;
    }

    public String getAddress_line() {
        return address_line;
    }

    public String getZip_code() {
        return zip_code;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public Position getCountry() {
        return country;
    }

    public Position getCity() {
        return city;
    }

    public Position getState() {
        return state;
    }
}
