package com.example.mercadolibreprueba.models.SearchResponse;

public class Paging {

    private int
            total,
            ofset,
            limit,
            primary_results;

    public int getTotal() {
        return total;
    }

    public int getOfset() {
        return ofset;
    }

    public int getLimit() {
        return limit;
    }

    public int getPrimaryResults() {
        return primary_results;
    }
}
