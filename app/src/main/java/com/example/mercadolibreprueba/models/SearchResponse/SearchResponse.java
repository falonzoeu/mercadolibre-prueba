package com.example.mercadolibreprueba.models.SearchResponse;

import java.util.List;

public class SearchResponse {

    private Paging paging;
    private List<SearchResultProduct> results;

    public Paging getPaging() {
        return paging;
    }

    public List<SearchResultProduct> getResults() {
        return results;
    }
}
