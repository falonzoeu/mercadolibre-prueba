package com.example.mercadolibreprueba.models;

import java.io.Serializable;

public class Attributes implements Serializable {

    private String
            name,
            value_name,
            id,
            attribute_group_id,
            attribute_group_name;

    private Long source;


    public String getName() {
        return name;
    }

    public String getValueName() {
        return value_name;
    }

    public String getId() {
        return id;
    }

    public String getAttributeGroupId() {
        return attribute_group_id;
    }

    public String getAttributeGroupName() {
        return attribute_group_name;
    }

    public Long getSource() {
        return source;
    }
}
