package com.example.mercadolibreprueba.models.SearchResponse;

import com.example.mercadolibreprueba.models.Attributes;

import java.io.Serializable;
import java.util.List;

public class SearchResultProduct implements Serializable {

    private String
            id,
            title,
            condition,
            thumbnail,
            original_price,
            catalog_product_id,
            permalink;

    public String getPermalink() {
        return permalink;
    }

    private int
            available_quantity,
            sold_quantity;

    private float price;

    private boolean
            accepts_mercadopago;

    private List<Attributes> attributes;

    private List<String> tags;

    private Address address;
    private Shipping shipping;
    private SellerAddress seller_address;



    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCondition() {
        return condition;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getOriginalPrice() {
        return original_price;
    }

    public float getPrice() {
        return price;
    }

    public int getAvailableQuantity() {
        return available_quantity;
    }

    public int getSoldQuantity() {
        return sold_quantity;
    }

    public boolean acceptsMercadopago() {
        return accepts_mercadopago;
    }

    public List<Attributes> getAttributes() {
        return attributes;
    }

    public List<String> getTags() {
        return tags;
    }

    public Address getAddress() {
        return address;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public SellerAddress getSellerAddress() {
        return seller_address;
    }

    public String getCatalogProductId() {
        return catalog_product_id;
    }
}
